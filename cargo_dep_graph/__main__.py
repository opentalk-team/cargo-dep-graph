# Copyright (C) 2024 Guido Berhoerster <guido+freiesoftware@berhoerster.name>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from pathlib import Path
from enum import Enum
import os
import configparser
import argparse
import sys

from .cargo_dep_graph import DependencyGraph, PackageStatus, ColorScheme


class EnumAction(argparse.Action):
    def __init__(self, **kwargs):
        type_ = kwargs.pop("type", None)

        if type_ is None or not issubclass(type_, Enum):
            raise ValueError("type must be an Enum")

        names = tuple(e.name for e in type_)
        upper = tuple(e.name.upper() for e in type_)
        if names != upper:
            raise ValueError("enum names must be uppercase")
        kwargs.setdefault("choices", [name.lower() for name in names])

        super().__init__(**kwargs)

        self._enum = type_

    def __call__(self, parser, namespace, value, option_string=None):
        setattr(namespace, self.dest, self._enum[value.upper()])


class EnumAppendAction(EnumAction):
    def __init__(self, **kwargs):
        self._is_default = True

        super().__init__(**kwargs)

    def __call__(self, parser, namespace, value, option_string=None):
        if self._is_default:
            s = set()
            self._is_default = False
        else:
            s = set(getattr(namespace, self.dest))
        s.add(self._enum[value.upper()])
        setattr(namespace, self.dest, list(s))


def read_config(config_path):
    config_converters = {
        "list": (
            lambda v: [i.strip() for i in v.split(",")] if len(v) > 0 else []
        ),
    }
    cp = configparser.ConfigParser(converters=config_converters)
    cp.read(config_path)
    colorscheme = {
        "node_default": cp.get(
            "color-scheme", "node_default", fallback="#dddddd"
        ),
        "node_unknown": cp.get(
            "color-scheme", "node_unknown", fallback="#dddddd"
        ),
        "node_compatible": cp.get(
            "color-scheme", "node_compatible", fallback="#dddddd"
        ),
        "node_incompatible": cp.get(
            "color-scheme", "node_incompatible", fallback="#dddddd"
        ),
        "clusters": cp.getlist(
            "color-scheme", "clusters", fallback=["#eeeeee"]
        ),
    }
    cluster_filter = [
        PackageStatus[v.upper()]
        for v in cp.getlist(
            "main", "filter", fallback=["unknown", "incompatible"]
        )
    ]
    return {
        "main": {
            "with-debian": cp.getboolean("main", "with_debian", fallback=False),
            "with-dev": cp.getboolean("main", "with_dev", fallback=False),
            "cluster-filter": cluster_filter,
        },
        "color_scheme": ColorScheme(**colorscheme)
    }


cargo = os.getenv("CARGO", "cargo")
config_path = (
    Path(os.getenv("XDG_CONFIG_HOME", Path.home() / ".config")) /
    "cargo-dep-graph" /
    "cargo-dep-graph.conf"
)
cache_path = (
    Path(os.getenv("XDG_CACHE_HOME", Path.home() / ".cache")) /
    "cargo-dep-graph"
)

try:
    config = read_config(config_path)
except (configparser.Error, ValueError) as error:
    print(error, file=sys.stderr)
    sys.exit(1)

parser = argparse.ArgumentParser()
parser.add_argument(
    "-c", "--clusters",
    type=int, default=0,
    help="divide crates into specified number of clusters"
)
parser.add_argument(
    "-d", "--with-debian",
    action="store_true", default=config["main"]["with-debian"],
    help="include status of corresponding Debian packages"
)
parser.add_argument(
    "-e", "--with-dev",
    action="store_true", default=config["main"]["with-dev"],
    help="include development dependencies"
)
parser.add_argument(
    "-F", "--cluster-filter",
    type=PackageStatus,
    default=config["main"]["cluster-filter"],
    action=EnumAppendAction,
    help="filters used to divide crates into specified number of clusters"
)
parser.add_argument(
    "-f", "--graph-format",
    type=str, default="dot", choices=["svg", "pdf", "dot"],
    help="output format"
)
parser.add_argument(
    "-o", "--output",
    type=Path, help="output file name"
)
parser.add_argument(
    "-t", "--type",
    type=str, choices=["list", "graph"], default="list",
    help="output type"
)
parser.add_argument(
    "project_path",
    type=Path, help="project path"
)
args = parser.parse_args()

if (
        os.isatty(sys.stdout.fileno()) and
        args.type == "graph" and
        args.graph_format == "pdf" and
        args.output is None
    ):
    print("cannot output PDF format to standard out", file=sys.stderr)
    sys.exit(1)

dg = DependencyGraph(
    path=args.project_path, cargo=cargo, cache_path=cache_path,
    with_dev=args.with_dev, with_debian=args.with_debian,
    color_scheme=config["color_scheme"]
)
dg.build()

output_file = args.output if args.output is not None else sys.stdout.fileno()
with open(output_file, "wb", closefd=args.output is not None) as f:
    if args.type == "list":
        data = dg.write_csv(f, args.clusters, cluster_filter=args.cluster_filter)
    elif args.type == "graph":
        data = dg.write_graph(
            f, args.graph_format, args.clusters, cluster_filter=args.cluster_filter
        )
