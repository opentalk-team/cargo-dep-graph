# Copyright (C) 2024 Guido Berhoerster <guido+freiesoftware@berhoerster.name>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from dataclasses import dataclass
from typing import ClassVar, List
from enum import Enum, auto
from graphlib import TopologicalSorter, CycleError
from pathlib import Path
import csv
import io
import re
import subprocess

from psycopg import sql
import graphviz
import psycopg
import psycopg.rows
import diskcache


POSTGRES_CONNINFO = (
    "postgresql://udd-mirror:udd-mirror@udd-mirror.debian.net/udd"
)
CACHE_TTL = 12 * 60 * 60  # s


class PackageStatus(Enum):
    UNKNOWN = auto()
    COMPATIBLE = auto()
    INCOMPATIBLE = auto()


@dataclass
class Node:
    _DEBIAN_EPOCH_RE: ClassVar[re.Pattern] = (
        re.compile(r"(?:\d+:)?(.+)", re.ASCII)
    )
    _DEBIAN_VERSION_RE: ClassVar[re.Pattern] = (
        re.compile(
            r"(\d+)(?:\.(\d+)(?:(?=[.+:~-])|$))?(?:\.(\d+)(?:(?=[.+:~-])|$))?",
            re.ASCII
        )
    )
    crate_name: str
    crate_version: str
    package_name: str = None
    package_version: str = None

    @property
    def name(self):
        return f"{self.crate_name} v{self.crate_version}"

    def _debian_version_to_semver(self):
        # remove epoch and debian revision
        m = self._DEBIAN_EPOCH_RE.match(self.package_version)
        upstream_rev = m.group(1).rsplit("-", 1)[0]
        # extract major, minor, patch parts (digits followed by ".+:~-" or end
        # of string and preceded by "." except for the major part)
        m = self._DEBIAN_VERSION_RE.match(upstream_rev)
        # reassemble parts
        return ".".join([n if n is not None else "0" for n in m.groups()])

    def _is_debian_compatible(self):
        if self.package_version is None:
            return False

        crate_parts = [int(n) for n in self.crate_version.split(".")]
        debian_parts = [
            int(n) for n in self._debian_version_to_semver().split(".")
        ]
        if self.crate_version.startswith("0."):
            return (
                crate_parts[0] == debian_parts[0] and
                crate_parts[1] == debian_parts[1]
            )
        return (
            crate_parts[0] == debian_parts[0] and
            crate_parts[1] <= debian_parts[1]
        )

    def package_status(self):
        if self.package_version is not None:
            if self._is_debian_compatible():
                return PackageStatus.COMPATIBLE
            else:
                return PackageStatus.INCOMPATIBLE
        return PackageStatus.UNKNOWN

    def __hash__(self):
        return hash(self.name)


@dataclass
class Edge:
    head: str
    tail: str


@dataclass
class ColorScheme:
    node_default: str
    node_unknown: str
    node_compatible: str
    node_incompatible: str
    clusters: List[str]


class DependencyGraph:
    _CARGO_ARGS = [
        "tree", "--quiet", "--charset", "ascii", "--prefix", "depth"
    ]
    _CARGO_TREE_RE = (
        re.compile(
            r"(?P<depth>\d+)(?P<name>[^ ]+) v(?P<version>\d+(?:\.\d+)*)",
            re.ASCII
        )
    )

    def __init__(
        self, path=".", cargo="cargo", cache_path=None, with_dev=False,
        with_debian=False, color_scheme=None
    ):
        self.path = Path(path)
        self.cache_path = cache_path
        self.nodes = {}
        self.edges = []
        self.with_debian = with_debian
        if color_scheme is None:
            color_scheme = ColorScheme(
                "#dddddd",
                "#dddddd",
                "#dddddd",
                "#dddddd",
                ["#eeeeee"],
            )
        self.color_scheme = color_scheme
        edges_args = ["normal", "build"]
        if with_dev:
            edges_args.append("dev")
        self._cargo_cmd = (
            [cargo] + self._CARGO_ARGS + ["--edges", ",".join(edges_args)]
        )

    def _parse_cargo_tree(self, input_):
        self.nodes = {}
        self.edges = []
        stack = []
        depth = 0
        for line in input_:
            m = self._CARGO_TREE_RE.match(line)
            if not m:
                assert line.strip() == ""
                continue
            new_depth = int(m["depth"])
            assert 0 <= new_depth <= depth + 1
            node = Node(m["name"], m["version"])

            if new_depth <= depth:
                stack = stack[0:new_depth]
            stack.append(node)

            if len(stack) > 1:
                self.edges.append(Edge(stack[-2].name, stack[-1].name))
            self.nodes[node.name] = node

            depth = new_depth

    def _find_debian_package(self, conn, node, release, is_new):
        if is_new:
            query = sql.SQL(
                """
                SELECT source, version
                FROM new_sources
                WHERE source in (%(source1)s, %(source2)s);
                """
            )
        else:
            query = sql.SQL(
                """
                SELECT source, version
                FROM sources
                WHERE
                    source in (%(source1)s, %(source2)s) AND
                    release = %(release)s
                """
            )
        package_name = f'rust-{node.crate_name.replace("_", "-")}'
        # see https://wiki.debian.org/Teams/RustPackaging/Policy#Package_dependencies
        shortversion = (
            ".".join(node.crate_version.split(".")[0:2])
            if node.crate_version.startswith("0.") else
            node.crate_version.split(".", 1)[0]
        )
        source_names = (package_name, f'{package_name}-{shortversion}')
        with conn.cursor(row_factory=psycopg.rows.tuple_row) as curs:
            curs.execute(
                query,
                {
                    "source1": source_names[0],
                    "source2": source_names[1],
                    "release": release,
                }
            )
            result = curs.fetchone()
            if result:
                result = (result[0].decode("utf-8"), result[1].decode("utf-8"))
            return result

    def _update_debian_package_data(self, release="sid"):
        with (
            psycopg.connect(POSTGRES_CONNINFO) as conn,
            diskcache.Cache(self.cache_path) as cache
        ):
            for node in self.nodes.values():
                for result in (
                    cache.get(node.crate_name),
                    self._find_debian_package(conn, node, release, False),
                    self._find_debian_package(conn, node, release, True)
                ):
                    if result:
                        node.package_name, node.package_version = result
                        break
                cache.add(node.crate_name, result, expire=CACHE_TTL)

    def build(self):
        with subprocess.Popen(
            self._cargo_cmd, cwd=self.path.absolute(), stdout=subprocess.PIPE,
            text=True
        ) as proc:
            self._parse_cargo_tree(proc.stdout)
        if proc.returncode:
            raise subprocess.CalledProcessError(proc.returncode, proc.args)
        if self.with_debian:
            self._update_debian_package_data()

    def _sorted_nodes(self):
        edges = self.edges.copy()

        # to sort topologically, if there are cycles, delete last edge of the
        # cycle and try again until it suceeds
        while True:
            ts = TopologicalSorter()
            for node in self.nodes.values():
                ts.add(node)
            for edge in edges:
                ts.add(self.nodes[edge.tail], self.nodes[edge.head])
            try:
                node_list = [*ts.static_order()]
            except CycleError as e:
                cycle = e.args[1]
                head = cycle[-2].name
                tail = cycle[-1].name
                edges = [
                    edge for edge in edges
                    if edge.head != head or edge.tail != tail
                ]
            else:
                break
        node_list.reverse()
        return node_list

    def _chunked_nodes(self, n_chunks, cluster_filter):
        node_list = self._sorted_nodes()
        if n_chunks == 0:
            return node_list

        matching = 0
        for node in node_list:
            if node.package_status() in cluster_filter:
                matching += 1

        chunked_node_list = [[] for i in range(n_chunks)]
        q, r = divmod(matching, n_chunks)
        chunks_matching = [
            q + 1 if i <= r else q for i in range(1, n_chunks+1)
        ]
        chunk = 0
        chunk_matching = 0
        for node in node_list:
            if node.package_status() in cluster_filter:
                chunk_matching += 1
                if chunk_matching > chunks_matching[chunk]:
                    chunk += 1
                    chunk_matching = 1
            chunked_node_list[chunk].append(node)

        return chunked_node_list

    def _add_graph_node(self, graph, node):
        status = node.package_status()
        label = node.name
        if self.with_debian:
            if status != PackageStatus.UNKNOWN:
                label += (
                    f"\nDebian: {node.package_name} {node.package_version} "
                    f"({status.name.lower()})"
                )
            else:
                label += f"\nDebian: {status.name.lower()}"
            if status == PackageStatus.COMPATIBLE:
                color = self.color_scheme.node_compatible
            elif status == PackageStatus.INCOMPATIBLE:
                color = self.color_scheme.node_incompatible
            else:
                color = self.color_scheme.node_unknown
        else:
            color = self.color_scheme.node_default
        graph.node(
            node.name, label=graphviz.escape(label),
            _attributes={"color": color}
        )

    def write_graph(
        self, f, format, n_clusters,
        cluster_filter=[PackageStatus.UNKNOWN, PackageStatus.INCOMPATIBLE]
    ):
        dot = graphviz.Digraph(
            "cargo-tree",
            graph_attr={
                "rankdir": "RL",
                "newrank": "true",
                "fontname": "Helvetica"
            },
            node_attr={
                "shape": "box",
                "style": "rounded,filled",
                "fontname": "Helvetica"
            },
            edge_attr={
                "dir": "back",
                "fontname": "Helvetica"
            },
            engine="dot",
        )
        if self.with_debian and n_clusters > 0:
            for i, chunk in enumerate(
                self._chunked_nodes(n_clusters, cluster_filter)
            ):
                with dot.subgraph(name=f"cluster-{i+1}") as cluster:
                    color = (
                        self.color_scheme.clusters[
                            i % len(self.color_scheme.clusters)
                        ]
                    )
                    cluster.attr(
                        label=f"Cluster {i+1}", style="rounded,filled",
                        color=color
                    )
                    for node in chunk:
                        self._add_graph_node(cluster, node)
        else:
            for node in self.nodes.values():
                self._add_graph_node(dot, node)
        for edge in self.edges:
            dot.edge(edge.head, edge.tail)

        if format == "dot":
            f.write(dot.source.encode("utf-8"))
        else:
            f.write(dot.pipe(format=format))

    def _csv_row(self, node, cluster):
        row = []
        if cluster is not None:
            row.append(cluster)
        row += [
            node.crate_name,
            node.crate_version,
        ]
        if self.with_debian:
            row += [
                node.package_name if node.package_name is not None else "",
                node.package_version
                if node.package_version is not None else "",
                node.package_status().name.lower(),
            ]
        return row

    def write_csv(
        self, f, n_clusters,
        cluster_filter=[PackageStatus.UNKNOWN, PackageStatus.INCOMPATIBLE]
    ):
        with io.TextIOWrapper(f, encoding="utf-8", newline="") as tf:
            csv_writer = csv.writer(tf)
            if self.with_debian and n_clusters > 0:
                for i, chunk in enumerate(
                    self._chunked_nodes(n_clusters, cluster_filter)
                ):
                    for node in chunk:
                        csv_writer.writerow(self._csv_row(node, i+1))
            else:
                for node in self._sorted_nodes():
                    csv_writer.writerow(self._csv_row(node, None))
