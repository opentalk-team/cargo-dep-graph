# cargo-dep-graph

cargo-dep-graph is a utility for visualizing the dependencies of Rust projects
which use the Cargo package manager.  It can optionally display whether a
compatible version of each dependency is packaged in Debian.  Missing or
incompatible dependencies in Debian may be grouped in a given number of
clusters of approximately equal size in order to divide up the work of
updating or creating the packages in Debian.

cargo-dep-graph can produce graphs using GraphViz in PDF or SVG format or
output the source.  Alternatively, the dependencies can also be converted to
an ordered list in CSV format.

## Installation

The easiest way to install the required dependencies is using `pip` in a
virtual environment.  Run the following commands from inside the project
directory:

    python3 -m venv .venv
    . ./.venv/bin/activate
    pip install -r requirements.txt

## Usage

The utility can the be invoked as follows:

    python3 -m cargo_dep_graph ... project_path

All supported command line options and their help texts can be shown using:

    python3 -m cargo_dep_graph --help

cargo-dep-graph can be configured through a configuration file in
`$XDG_CONFIG_HOME/cargo-dep-graph/cargo-dep-graph.conf`.  The included file
`cargo-dep-graph.conf.example` can be taken as a starting point for
customizations.

### Examples

Generate a graph of all dependencies in SVG format:

    python3 -m cargo_dep_graph --type=graph --graph-format=svg --output=project.svg /path/to/project

Generate a graph of all dependencies including development dependencies and
the status of corresponding Debian packages in PDF format:

    python3 -m cargo_dep_graph --type=graph --graph-format=pdf --output=project.pdf --with-dev --with-debian /path/to/project

Generate the same graph as above but group dependencies which are missing from
Debian into four clusters:

    python3 -m cargo_dep_graph --type=graph --graph-format=pdf --output=project.pdf --clusters=4 --cluster-filter=unknown --with-dev --with-debian /path/to/project

Generate an ordered list in CSV format with the same information as the above
graph:

    python3 -m cargo_dep_graph --type=list --output=project.csv --clusters=4 --cluster-filter=unknown --with-dev --with-debian /path/to/project

## License

Except otherwise noted, all code is distributed under the following license
terms:

Copyright (C) 2024 Guido Berhoerster <guido+freiesoftware@berhoerster.name>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
